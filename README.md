# man2pdf

Pequeño script para visualizar o guardar manpages a pdf

## Uso

man2pdf \[Capitulo\] \[Manpage\] \[Archivo\]

El **unico** campo obligatorio es obviamente el del manpage.
Si no se proporciona el nombre del archivo, el manpage se abrira 
en una ventana de visor de pdf que desee. Para esto, solo hay que
cambiar la variable VIEWER (linea 5 del script) al que mas le agrade.

## Autor

Matias Linares <mlm0111@famaf.unc.edu.ar>
